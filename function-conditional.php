<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php


    // NOMOR 1
    echo "<h3> Soal No 1 Greetings </h3>";
    function greetings($nama)
    {
        echo "Halo " . $nama . ", Selamat Datang di Sanbercode! <br>";
    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("Abdul");

    echo "<br>";
    // END NOMOR 1

    // NOMOR 2
    echo "<h3>Soal No 2 Reverse String</h3>";

    function reverse($kata)
    {
        $tampung = "";
        for ($i = strlen($kata) - 1; $i >= 0; $i--) {
            $tampung .=  $kata[$i];
        }
        return $tampung;
    }
    function reverseString($namabalik)
    {
        $balik_string = reverse($namabalik);
        echo $balik_string . "<br>";
    }

    reverseString("abduh");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";
    // END NOMOR 2

    // NOMOR 3
    echo "<h3>Soal No 3 Palindrome </h3>";
    // Code function di sini
    function palindrome($poli)
    {
        $balik_kata = reverse($poli);
        if ($balik_kata == $poli) {
            echo $poli . " => True <br>";
        } else {
            echo $poli . " => False <br>";
        }
    }
    palindrome("civic"); // true
    palindrome("nababan"); // true
    palindrome("jambaban"); // false
    palindrome("racecar"); // true
    // END NOMOR 3


    // NOMOR 4
    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    function tentukan_nilai($nilai)
    {
        if ($nilai >= 80 && $nilai <= 100) {
            echo "Sangat Baik <br>";
        } else if ($nilai >= 70 && $nilai <= 85) {
            echo "Baik <br>";
        } else if ($nilai >= 60 && $nilai <= 70) {
            echo "Cukup <br>";
        } else {
            echo "Kurang <br>";
        }
    }

    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang
    // END NOMOR 4

    ?>

</body>

</html>