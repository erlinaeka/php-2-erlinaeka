<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>

<body>
    <h1>Berlatih Looping</h1>

    <!-- INI NOMOR 1 -->
    <?php
    echo "<h3>Soal No 1 Looping I Love PHP</h3>";

    echo "<h4>Output :</h4>";
    echo "<h5>LOOPING PERTAMA :</h5>";
    for ($i = 2; $i <= 20; $i += 2) {
        echo $i . " - I Love PHP <br>";
    }

    echo "<h5>LOOPING KEDUA :</h5>";
    for ($j = 20; $j > 0; $j -= 2) {
        echo $j . " - I Love PHP <br>";
    }
    // END NOMOR 1


    // NOMOR 2
    echo "<h3>Soal No 2 Looping Array Modulo </h3>";

    $numbers = [18, 45, 29, 61, 47, 34];
    echo "array numbers: ";
    print_r($numbers);
    // Lakukan Looping di sini
    foreach ($numbers as $number) {
        $tambahlima[] = $number + 5;
    }
    echo "<br>";
    echo "Array tambah 5 adalah: ";
    print_r($tambahlima);
    echo "<br>";

    // END NOMOR 2


    // NOMOR 3
    echo "<h3> Soal No 3 Looping Asociative Array </h3>";
    $items = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'],
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach ($items as $key => $item) {
        $items = [
            "id" => $item[0],
            "name" => $item[1],
            "price" => $item[2],
            "description" => $item[3],
            "source" => $item[4]
        ];
        print_r($items);
        echo "<br>";
    }
    // END NOMOR 3


    // NOMOR 4
    echo "<h3>Soal No 4 Asterix </h3>";

    echo "Asterix: ";
    echo "<br>";
    for ($l = 5; $l >= 0; $l--) {
        for ($m = 5; $m > $l; $m--) {
            echo "*";
        }
        echo "<br>";
    }
    // END NOMOR 4

    ?>

</body>

</html>
